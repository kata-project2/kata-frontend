FROM node:latest

WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY package.json /app/package.json
RUN npm install
RUN npm install -g @angular/cli@7.3.9

COPY . /app

# Use the shell form of CMD to ensure environment variables are properly inherited
CMD ng serve --host 0.0.0.0 --port 4200

